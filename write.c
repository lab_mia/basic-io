#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include "estructuras.h"

void format(FILE * file, int size){
/* Esta funcion recibe un puntero a un archivo y escribe SIZE KB de caracteres
 * nulos en el archivo.
 * El paramtero size esta en KB. Para crear un archivo de 1KB, enviar 1.
 * Para crear uno de 2MB enviar 2000 y asi.
 *
 * Esta funcion no utiliza fseek por lo que escribe dichos caracteres donde sea
 * que este el puntero del archivo actualmente, siempre hay que asegurarse que el puntero
 * este al principio del archivo o en cual sea la posicion de interes.
 * */
	char buffer[1024];
	for(int i = 0; i<1024; i++) buffer[i]='\0';
	for( int i = 0; i < size; i++)
		fwrite(&buffer,1024,1,file);
}

int main(void){
	date *object=malloc(sizeof(date));
	alumno *Renato=malloc(sizeof(alumno));
	strcpy(Renato->nombre ,"Renato Flores");
	strcpy(Renato->correo , "renatojosuefloresperez@gmail.com");
	strcpy(Renato->cui , "3700401630101");
	Renato->carnet = 201709244;

	strcpy(object->day,"Clase 2 laboratorio MIA");
	object->month=2;
	object->year=2020;

	FILE * file= fopen("output", "wb"); /* Usar wb para crear un nuevo archivo y truncar el viejo. Usar rb+ si se desea sobreescribir
	solo una parte del archivo. */
	if (file != NULL) {
            /* format(file,1); -- Creamos un archivo vacio de 1KB */ 
	    fseek(file, 0, SEEK_SET); /* Nos aseguramos de que el puntero este al principio del archivo */
	    fwrite(object, sizeof(date), 1, file); /* Escribimos la estructura date */
	    fwrite(Renato, sizeof(alumno), 1, file); /* Escribimos la estructura alumno */
	    fclose(file);
	}
	return 0;
}
