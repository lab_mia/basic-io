# Basic I/O en C

En C, pueden escribir y leer un archivo binario utilizando el siguiente ejemplo. Las funciones escenciales para este tipo de operaciones
son fseek, fread, fwrite, fopen, fclose. No olviden fclose! <br>
El ejemplo incluye dos archivos fuente: write.c y read.c ambos utilizan el mismo header file donde se definen algunas estructuras triviales
para el ejemplo. Sin embargo, ambos archivos se compilan por separado y crean 2 ejecutables separados. Pueden compilar la aplicación de
escritura con el comando:

```zsh
  cc write.c -o write
```
Pueden compilar el de lectura con el comando:
```zsh
  cc read.c -o read
```

Para ejecutar un ejecutable u otro, pueden utilizar el comando:

```zsh
 ./write #O puede utilizar ./read según sea el caso.
```

Si prefieren utilizar un único comando, para compilar y ejecutar ambos programas uno después del otro, pueden utilizar:

```zsh
F=write; cc $F.c -o $F && ./$F; F=read; cc $F.c -o $F && ./$F
```

Por conveniencia, pueden ejecutar el comando de arriba con el comando: ```./run``` <br>

Para visualizar el archivo binario que genera el programa de escritura (output por default) pueden abrir el archivo con vim y ejecutar
el comando: ```%!xxd``` para obtener la visualización hex que se vió en el laboratorio. Este comando pueden utilizarlo siempre que deseen
inspeccionar un archivo binario.
