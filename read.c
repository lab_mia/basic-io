#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <stdio.h>
#include "estructuras.h"

int main(void){
    alumno *persona=malloc(sizeof(alumno));
    FILE * file= fopen("output", "rb");
    if (file != NULL) {
        fseek(file, sizeof(date), SEEK_SET);
        fread(persona, sizeof(alumno), 1, file);
        fclose(file);
    }
    printf("%s, %d\n",persona->nombre,persona->carnet);
}
