typedef struct tagDate {
    char day[80];
    int month;
    int year;
} date;

typedef struct tagAlumno {
    char nombre[20];
    int carnet;
    char cui[13];
    char correo[50];
} alumno;
